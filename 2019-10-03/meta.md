## Integrantes del grupo.

... ✏️

- Carlos Padilla, Sergio Noh Puch
- Ricardo Amendolia, Sergio Dominguez

## Desglose de tareas

- Implementar la función `Diseño de espacio de juego`.

Asignado a Dominguez, Sergio

- Implementar la función `renderizar hero y movimiento`.

Asignado a Noh Puch, Sergio

- Implementar la función `renderizar enemigos`.

Asignado a Amendolia, Ricardo

- Implementar la función `Movimiento de Enemigos`.

Asignado a Padilla, Carlos

- Implementar la función `Disparo de bullet y destruccion de target`.

Asignado a Padilla, Carlos

- Implementar la función `Disparo de enemigos y muerte del hero`.

Asignado a Padilla, Carlos

- Implementar la función `respawn del hero`.

Asignado a Padilla, Carlos

- Implementar la función `Vidas`.

- Asignado a Noh Puch, Sergio

class Hero {
  constructor() {
    this.parent = null;
    this.hero = null;
    this.pos = 0;
  }

  /**
   * render the hero in the DOM
   * @param {} parent
   */
  render(parent) {
    const heroIcon = document.createElement("img");
    heroIcon.src = sprites.player;

    this.hero = document.createElement("div");
    this.hero.setAttribute("class", "hero");
    this.hero.style.marginLeft = `${this.pos}px`;

    document.body.addEventListener("keydown", e => this.getKeyAndAction(e));

    this.hero.appendChild(heroIcon);

    this.parent = parent;

    this.parent.appendChild(this.hero);
  }

  //check if the hero is the limit of the container
  isInLeftLimit() {
    const limitPx = this.getLimitPx();

    if (this.pos >= 0) {
      return this.pos >= limitPx;
    }
  }

  //check if the hero is the limit of the container
  isInRightLimit() {
    const limitPx = this.getLimitPx();

    if (this.pos <= 0) {
      return this.pos <= limitPx * -1;
    }
  }

  getLimitPx() {
    const heroWidth = 80;
    const { width } = this.parent.getBoundingClientRect();

    return width - heroWidth;
  }

  getKeyAndAction(e) {
    const movePx = 30;

    const getMove = {
      //letter D
      "68": () => {
        if (!this.isInLeftLimit()) {
          this.pos += movePx;
          this.hero.style.marginLeft = `${this.pos}px`;
        }
      },
      //letter A
      "65": () => {
        if (!this.isInRightLimit()) {
          this.pos -= movePx;
          this.hero.style.marginLeft = `${this.pos}px`;
        }
      },
      //letter W
      "87": () => {
        const { x, y } = this.hero.getBoundingClientRect();

        new Bullet({
          target: "enemy",
          direction: "up",
          coords: { x, y }
        }).shoot();
      }
    };

    const move = getMove[e.keyCode];
    if (move && !this.hero.classList.contains("die")) {
      move();
    }
  }
}

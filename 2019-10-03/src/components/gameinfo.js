class GameInfo {
  constructor() {
    this.herolifes = null;
  }
  render() {
    const heroLifesIcon = document.createElement("img");
    heroLifesIcon.src = sprites.player;

    this.herolifes = document.createElement("div");
    this.herolifes.setAttribute("class", "lifes");
    this.herolifes.appendChild(heroLifesIcon);
  }
}

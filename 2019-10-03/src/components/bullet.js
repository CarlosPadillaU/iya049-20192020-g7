class Bullet {
  constructor(props = { target, direction: "up", coords: { x: 0, y: 0 } }) {
    this.target = props.target;
    this.direction = props.direction;
    this.coords = props.coords;
  }

  //130
  //700
  shoot(onColision) {
    const movePx = 20;
    let pos = this.coords.y;

    const div = document.createElement("div");
    div.setAttribute("class", "bullet");
    div.style.top = `${pos}px`;
    div.style.left = `${this.coords.x + 40}px`;

    document.body.appendChild(div);

    const hitTarget = () => {
      const targets = document.querySelectorAll(`.${this.target}:not(.die)`);

      for (let target of targets) {
        const { x, y, height, width } = target.getBoundingClientRect();
        const bulletX = div.getBoundingClientRect().x;
        const bulletY = div.getBoundingClientRect().y;

        const bulletWidth = 10;
        const bulletHeight = 25;

        if (
          x < bulletX + bulletWidth &&
          x + width > bulletX &&
          y < bulletY + bulletHeight &&
          height + y > bulletY
        ) {
          div.remove();

          target.classList.add("die");
          // target.children[0].remove();

          if (onColision) onColision();
        }
      }
    };

    let interval = 0;
    const moveTo = {
      up: () => {
        pos -= movePx;
        if (pos <= 130) {
          clearInterval(interval);
          div.remove();
        }
      },
      down: () => {
        pos += movePx;
        if (pos >= 700) {
          clearInterval(interval);
          div.remove();
        }
      }
    };

    interval = setInterval(() => {
      moveTo[this.direction]();

      div.style.top = `${pos}px`;
      hitTarget();
    }, 100);
  }
}
